# Awesome Web-Development tools (PHP)

This is a personal list of awesome tools which improves my workflows.

## Software

* [Homebrew](https://brew.sh/) - (**macOS**) A simple and beautiful package manager for macOS, to install packages clean, like 'PHP', 'tree' and so on.
* [iTerm2](https://www.iterm2.com/) - (**macOS**) iTerm2 is a replacement for Terminal and the successor to iTerm.
* ['Oh My ZSH!'](http://ohmyz.sh/) - Oh-My-Zsh is an open source, community-driven framework for managing your ZSH configuration via your terminal, which comes bundled with a lot of helpful functions, helpers, plugins, themes, and a few things that make you shout. My tip: try the theme agnoster with iTerm2  ('non-ascii font' as setting)
* [PhpStorm](https://www.jetbrains.com/phpstorm/) - PhpStorm is a commercial, cross-platform IDE for PHP, HTML and JavaScript with on-the-fly code analysis, error prevention and automated refactorings for PHP and JavaScript code.
* [Visual Studio Code](https://code.visualstudio.com/) - Visual Studio Code is a streamlined code editor with support for development operations like debugging, task running and version control.

### Helpful Plugins for PhpStorm

#### Code Quality and Support

* [Php Inspections (EA Extended)](https://plugins.jetbrains.com/plugin/7622-php-inspections-ea-extended-) - This is a static code analysis plugin for PHP.
* [Symfony Plugin](https://plugins.jetbrains.com/plugin/7219-symfony-plugin) - A lot of small useful tweaks for e.g. Symfony, doctrine and twig.

#### Themes

* [Material UI](https://plugins.jetbrains.com/plugin/8006-material-theme-ui) - Adds the [Material Theme](https://github.com/equinusocio/material-theme) look and bevaior to your IDE.

#### Utilities

* [Nyan Progress Bar](https://plugins.jetbrains.com/plugin/8575-nyan-progress-bar) - Pretty progress bar for PHPStorm. Just....amazing. Happy loading!
* [PHP Annotations](https://plugins.jetbrains.com/plugin/7320-php-annotations) - Adds support for PHP annotations. (Awesome in combination with the Symfony Plugin!)
* [PHP Toolbox](https://plugins.jetbrains.com/plugin/8133-php-toolbox) - Adds completion and typing for many libraries using dynamic returns types based on arguments.

### Helpful Plugins for Visual Studio Code

* [Beautify - HookyQR](https://marketplace.visualstudio.com/items?itemName=HookyQR.beautify) - Beautify code (JavaScript, JSON, CSS, Sass, and HTML) in place for VS Code
* [Docker - Microsoft](https://marketplace.visualstudio.com/items?itemName=PeterJausovec.vscode-docker) - Adds syntax highlighting, snippets, commands, hover tips, and linting for Dockerfile and docker-compose files.
* [Git History (git log) - Don Jayamanne](https://marketplace.visualstudio.com/items?itemName=donjayamanne.githistory) - View git log, file or line history inside of VS Code.
* [Go - lukehoban](https://marketplace.visualstudio.com/items?itemName=lukehoban.Go) - (*optional sidekick*) Go language support for Visual Studio Code.
* [PHP Debug - Felix Becker](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-debug) - Simple debug support for PHP with e.g. XDebug.
* [PHP IntelliSense - Felix Becker](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-intellisense) - Advanced autocompletion and refactoring support for PHP.
* [PHP Extension Pack - Felix Becker](https://marketplace.visualstudio.com/items?itemName=felixfbecker.php-pack) - Includes the most important extensions to get you started with PHP development in Visual Studio Code.